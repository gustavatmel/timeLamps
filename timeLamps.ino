// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include <Wire.h>
#include "RTClib.h"



RTC_DS1307 rtc;


unsigned long nowHour = 0;                // Counter für Stunden
unsigned long nowMinute = 0;              // Counter für Minuten
unsigned long nowSecond = 0;              // Counter für Sekunden
unsigned long nowMiliCount = 0;           // Counter für MiliSekunden

unsigned long delayLampOne = 0;             // Delay für erste Lampe 
unsigned long delayLampTwo = 0;             // Delay für zweite Lampe 
const unsigned long startDelay = 7000;      // min Delay
const unsigned long maxDelay = 8800 - startDelay;      // Maximales Delay
const unsigned long delayStep = maxDelay / 60 ;       // Delay Steps 


volatile boolean interruptCalled = false;
volatile unsigned long timeStamp = 0;
unsigned long delta = 0;

const byte interruptPin = 2;
int hourPins[] = {A0, A1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}; // Pins die Stunden sind
int pinCount = 12;                                        // Anzahl der Pins 
int thisPin = 0;

 

void setup () {

  Serial.begin(9600);

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }
 if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }



  DateTime now = rtc.now();       // Zeit von RTC laden
  
  nowHour = now.hour();           // Stunden, Minuten uns Sekunden Initialisieren
  nowMinute = now.minute();
  nowSecond = now.second();

  if (nowHour >= 12){
    nowHour = nowHour - 12;
  }

   

  for (thisPin = 0; thisPin < pinCount; thisPin++) {
    pinMode(hourPins[thisPin], OUTPUT);
  }                               // PINs als Output setzen  
  
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), zeroPointInterrupt, FALLING );

 

 
}

void zeroPointInterrupt() {
  timeStamp = micros();
  interruptCalled = true;
  
}

void loop () {

  
  if(interruptCalled) {
    //timeStamp = micros();
    interruptCalled = false;
    nowMiliCount ++;
  
    if (nowMiliCount >= 100){
      nowSecond++;
      nowMiliCount = 0;
    }

    if (nowSecond >= 60){
      nowMinute++;
      nowSecond = 0;
    }

    if (nowMinute >= 60) {
      nowHour++;
      Serial.print("Hour num: ");
      Serial.println(nowHour);
      nowMinute = 0;
      if (nowHour >= 12){
        nowHour = 0;
      }
    }
    
    delayLampOne = nowMinute * delayStep + startDelay;
    delayLampTwo = maxDelay - nowMinute * delayStep + startDelay;
    
  } else {
    delta = micros() - timeStamp;
    //debugMicros[i] = i;
  }

  if(delayLampOne <= delta) {
    digitalWrite(hourPins[nowHour], HIGH);
  }
  
  if(delayLampTwo <= delta) {
    if (nowHour <= 10) {
      digitalWrite(hourPins[nowHour+1], HIGH);
    }
    else {
      digitalWrite(hourPins[0], HIGH);
    }
    
  }

  if(delta >= 9500) {

    for (thisPin = 0; thisPin < pinCount; thisPin++) {
      digitalWrite(hourPins[thisPin], LOW);
    } 
  }
    
}

